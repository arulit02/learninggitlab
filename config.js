const config = {
    env: process.env.NODE_ENV,
    name: process.env.NODE_NAME
  };

  module.exports = config;